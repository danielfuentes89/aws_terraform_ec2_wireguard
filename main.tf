variable "my_access_key" {
  description = "Access-key-for-AWS"
  default     = "no_access_key_value_found"
}

variable "my_secret_key" {
  description = "Secret-key-for-AWS"
  default     = "no_secret_key_value_found"
}

provider "aws" {
  region     = "us-east-1"
  access_key = var.my_access_key
  secret_key = var.my_secret_key
}

resource "aws_instance" "ww-server" {
  ami           = "ami-036490d46656c4818"
  instance_type = "t2.micro"
  # user_data = file("./scripts/docker-script.sh")

  key_name   = "aws_key"

  tags = {
    Name = "docker-ubuntu20"
  }
  vpc_security_group_ids = [aws_security_group.instance.id]

  connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = file("./ssh/aws_key")
      timeout     = "4m"
   }

  # envia script de instalacion Docker
  provisioner "file" {
    source      = "./scripts/docker-script.sh"
    destination = "/tmp/docker-script.sh"
  }

  # ejecuta script de instalacion docker
  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x /tmp/docker-script.sh",
      "sudo sh /tmp/docker-script.sh",
    ]
  }

# creacion carpeta destino servicio
  provisioner "remote-exec" {
    inline = [
      "echo 'Hello fucking world!'",
      "pwd",
      "whoami",
      "mkdir -p lab/docker/wireguard",
    ]
  }
# provisiona docker compose
  provisioner "file" {
    source      = "./docker/wireguard/"
    destination = "/home/ubuntu/lab/docker/wireguard"
  }
# ejecuta instalacion servicio docker
  provisioner "remote-exec" {
    inline = [
      "cd /home/ubuntu/lab/docker/wireguard/",
      "pwd",
      "ls -ltr /home/ubuntu/lab/docker/wireguard/",
      "sudo chown -R 1001:1001 /home/ubuntu/lab/docker/wireguard/",
      "sudo docker-compose up -d",
      "sudo docker ps -a",
      "echo 'finaliza ok!'",
    ]
  }
# extrae peers desde volumen docker remoto
  provisioner "local-exec" {
    command = "scp -i './ssh/aws_key' -r -o 'ConnectTimeout 3' -o 'StrictHostKeyChecking no' -o 'UserKnownHostsFile /dev/null' ubuntu@${aws_instance.ww-server.public_ip}:lab/docker/wireguard/config/peer* ./docker/wireguard/config/"
    on_failure = continue
  }
# al destruir recurso elimina carpeta config con peers
  provisioner "local-exec" {
    when    = destroy
    command = "rm -rf ./docker/wireguard/config/*"
  }
}

resource "aws_security_group" "instance" {
  name = "terraform-tcp-security-group"

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 51820
    to_port     = 51820
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
 egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "aws_key"
  public_key = file("./ssh/aws_key.pub")
}

output "public_ip" {
  value = aws_instance.ww-server.public_ip
}

output "ssh_cmd" {
  value = "ssh -i './ssh/aws_key' ubuntu@${aws_instance.ww-server.public_ip}" 
}

output "scp_cmd" {
  value = "scp -i './ssh/aws_key' -r ubuntu@${aws_instance.ww-server.public_ip}:lab/docker/wireguard/config/peer* ./docker/wireguard/config/" 
}
# terraform apply -input=false -auto-approve

# terraform destroy -input=false -auto-approve
