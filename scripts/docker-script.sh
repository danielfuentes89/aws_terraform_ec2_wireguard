#!/bin/bash

sudo apt update -y
sudo apt upgrade -y
sudo apt-get remove docker docker-engine docker.io containerd runc -y
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release -y
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /bin/docker-compose
sudo chmod +x /bin/docker-compose
sudo systemctl enable docker.service
sudo systemctl start docker.service
echo "alias docker='sudo docker'" >> ~/.bashrc
echo "alias docker='docker'" >> ~/.bashrc
echo "alias docker-compose='sudo docker-compose'" >> ~/.bashrc
echo "alias docker-compose='docker-compose'" >> ~/.bashrc
sudo hostnamectl set-hostname docker