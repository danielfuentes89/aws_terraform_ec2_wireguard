# WireGuard VPN

Este proyecto configura una vpn **WireGuard** con Docker, en una instancia t2.micro (Ubuntu 20) sobre **AWS EC2** usando terraform.

## Requisitos
- [terraform cli](https://www.terraform.io/downloads)
- [Cuenta en AWS (la instancia t2.micro se incluye en la capa gratuita)](https://aws.amazon.com/free/)


## Pasos previos

- Crear directorio ./ssh y par de claves del proyecto en el. Por defecto el archivo "main.tf" invoca las llaves de nombre **aws_key**, por lo que debes crearlas con el mismo nombre o modificar el archivo main.tf donde se invoquen dichas claves.
- Crear archivo terraform.tfvars en la raíz del proyecto que contenga las siguientes variables.
	```sh
	my_access_key = "acces key previamente creada en aws"
	my_secret_key = "secret key previamente creada en aws"
	```
- (opcional) En archivo docker-compose.yml en **./docker/wireguard/** modificar la cantidad de **peers** (cantidad de tuneles), por defecto crea 6

## Comandos
- Para la vista previa de los recursos a crear.
    ```sh
    terraform plan
    ```
- Para la creación de los recursos **terraform apply** (**-input=false** deshabilita la interacción con el prompt y **-auto-approve** omite la aprobación del plan).
    ```sh
    terraform apply -input=false -auto-approve
    ```
- Para la destrucción de los recursos
    ```sh
    terraform destroy -input=false -auto-approvee
    ```

## Outputs
Al finalizar la ejecución, terraform extrae mediante scp desde el contenedor wireguard en el servidor recien instanciado, las carpetas **peers** con las configuraciones de cada tunel (qr png, .conf) y las almacena en **./docker/wireguard/config**

> **Nota:** en los outputs se indican los comandos ssh y scp configurados con la ip del server creado. Copia y pega en terminal para conectarte al server (ssh) o para traer las configuraciones de los peers (scp), en caso de no haber sido descargados.

#### se reciben mejoras, comparte.